package com.probe.demo.controller;

import com.probe.demo.model.Product;
import com.probe.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.http.HttpResponse;
import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(path = "/fetchall")
    public List<Product> getAllProducts() {
        List<Product> products = productService.getAllProducts();
        System.out.println("Size: " + products.size());
        return products;
    }

}
