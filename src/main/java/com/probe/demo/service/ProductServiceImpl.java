package com.probe.demo.service;

import com.probe.demo.model.Product;
import com.probe.demo.respository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> products = productRepository.findAll();
        return products;
    }

    @Override
    public Page<Product> getAllProductsWithPageable() {
        Pageable myPageableProducts = PageRequest.of(0, 10);
        Page<Product> products = productRepository.findAll(myPageableProducts);
        return products;
    }

    @Override
    public void generateProducts() {
        Product p = new Product(null, 123L, "My Fancy Product", 10, new BigDecimal(100), "Nothing to see here.");
        Product p1 = new Product(null, 124L, "My other Fancy Product", 1, new BigDecimal(1000), "Still nothing to see here.");

        List<Product> listOfProducts = new ArrayList<>();
        listOfProducts.add(p);
        listOfProducts.add(p1);

        System.out.println("Wir sind in generateProducts Function");

        productRepository.saveAll(listOfProducts);
    }

    @PostConstruct
    public void init() {
        generateProducts();
    }
}
