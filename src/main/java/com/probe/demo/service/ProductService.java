package com.probe.demo.service;

import com.probe.demo.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<Product> getAllProducts();

    Page<Product> getAllProductsWithPageable();

    void generateProducts();

}
